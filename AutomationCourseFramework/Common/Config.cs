﻿using System;
using System.Configuration;

namespace AutomationCourseFramework
{
	public class Config
	{
		public Config()
		{
			public static int ImplicitWaitTimeout { get; } = Int32.Parse(ConfigurationManager.AppSettings["ImplicitWaitTimeout"]);
			public static int PageLoadTimeout { get; } = Int32.Parse(ConfigurationManager.AppSettings["PageLoadTimeout"]);
		}
	}
}
	