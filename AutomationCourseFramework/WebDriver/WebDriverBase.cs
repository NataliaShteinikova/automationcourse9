﻿using OpenQA.Selenium;

namespace AutomationCourseFramework.WebDriver
{
    public abstract class WebDriverBase
    {
        protected IWebDriver WebDriver => DriverController.GetInstance();
    }
}
