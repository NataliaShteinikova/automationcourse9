﻿using System;
using AutomationCourseFramework.WebDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AutomationCourseFramework.Pages.PageElements
{
    public abstract class BaseElement : WebDriverBase
    {
        protected By Locator;
        protected BaseElement(By locator)
        {
            Locator = locator;
        }

        public string GetAttributeValue(string attributeName)
        {
            return WebDriver.FindElement(Locator).GetAttribute(attributeName);
        }

        public string Text => WebDriver.FindElement(Locator).Text;

        public void Click()
        {
            WebDriver.FindElement(Locator).Click();
        }

        public bool IsElementPresent
        {
            get
            {
                try
                {
                    WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
                    return WebDriver.FindElements(Locator).Count != 0;
                }
                finally
                {
                    WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
                }                               
            }
        }

        public void WaitForElementPresent()
        {
            new WebDriverWait(WebDriver, TimeSpan.FromSeconds(60))
                .Until(ExpectedConditions.ElementExists(Locator));
        }
    }
}
