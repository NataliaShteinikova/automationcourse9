﻿using System;
using System.Linq;
using AutomationCourseFramework.Pages.Assets;
using AutomationCourseFramework.Pages.PageElements;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.Archives.Tab
{
    public class ArchiveAsset : BasePage
    {
        private readonly int _index;
        private readonly string _infoBarTopLevelXpath;

        private string _topLevelXpath;
        private string TopLevelXpath
        {
            get
            {
                if (string.IsNullOrEmpty(_topLevelXpath))
                {
                    var href = new Label(By.XPath($"//div[@id='thumbnailWrapper']/ul[@class='thumbnails']/li[{_index}]/a")).GetAttributeValue("href");
                    var assetHrefPart = href.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last(); //To remove full link path(http://host/site/archive/...)
                    _topLevelXpath = $"//div[@id='thumbnailWrapper']/ul[@class='thumbnails']/li/a[contains(@href, '{assetHrefPart}')]/..";
                }

                return _topLevelXpath;
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                {
                    _name = new Label(By.XPath($"{TopLevelXpath}/a/div[@class='infobar']//span[@class='name']")).Text;
                }

                return _name;
            }
        }

        public ArchiveAsset(int index)
        {
            _index = index;
            _infoBarTopLevelXpath = $"{TopLevelXpath}/a/div[@class='infobar']";
        }

        public bool IsSelected => new Label(By.XPath(TopLevelXpath)).GetAttributeValue("class").Contains("selected");

        public void Select()
        {
            if (IsSelected) return;
            new Button(By.XPath($"{_infoBarTopLevelXpath}/i")).Click();
        }

        public void Deselect()
        {
            if (!IsSelected) return;
            new Button(By.XPath($"{_infoBarTopLevelXpath}/i")).Click();
        }

        public AssetDetailPage Open()
        {
            new Button(By.XPath($"{TopLevelXpath}/a/div[@class='imageContainer']")).Click();
            return new AssetDetailPage();
        }
    }
}
