﻿using System.Linq;
using AutomationCourseFramework.Pages.PageElements;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.Archives.Tab
{
    public class ArchiveGridPage : BasePage
    {

        public ArchiveAsset[] GetArchiveAssets()
        {
            const string assetsXpath = "//div[@id='thumbnailWrapper']/ul[@class='thumbnails']/li";
            
            return WebDriver
                .FindElements(By.XPath(assetsXpath))
                .Select((el, i) => new ArchiveAsset(i + 1))
                .ToArray();
        }

        public ArchiveAsset GetArchiveAsset(string assetName)
        {
            WaitForCondition(() =>
            {
                WaitForDisplay();
                if (GetArchiveAssets().Any(asset => asset.Name == assetName)) return true;

                WebDriver.Navigate().Refresh();
                return false;
            }, $"Asset with name {assetName} appearance timeout expired on the archive details page");

            return GetArchiveAssets().First(asset => asset.Name == assetName);
        }

        protected void WaitForDisplay()
        {
            WaitForCondition(() => new Label(By.XPath("//div[@id='thumbnailWrapper']//ul[@class='thumbnails' and @style='display: block;']")).IsElementPresent, "Archive assets appearance timeout expired");
        }
    }
}
