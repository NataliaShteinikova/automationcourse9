﻿using System;
using System.Threading;
using AutomationCourseFramework.Common;
using AutomationCourseFramework.Pages.Assets.AssetActions;
using AutomationCourseFramework.Pages.Assets.AssetInformation;
using AutomationCourseFramework.Pages.PageElements;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.Archives.Tab
{
    public class CridActionPanel : BasePage
    {
        public GridActionButton GetActionButton(AssetAction action)
        {
            WaitForActionPanelIsVisible();
            return new GridActionButton(action);
        }

        public void WaitForActionPanelIsVisible()
        {
            var actionPanelRoot = new Label(By.Id("grid-action-panel-wrapper"));
            WaitForCondition(() => actionPanelRoot.GetAttributeValue("class").Contains("grid-action-panel-open"), "Action panel appearence timeout expired");
            Thread.Sleep(500); // Wait for animation complete
        }

        public void SetRating(int stars)
        {
            if (stars < 0 && stars > 5)
                throw new Exception($"Couldn't set Rating value. Rating value can be witin 1-5. Current value is {stars}");

            GetActionButton(AssetAction.SetRating).Click();
            var ratingButton = new Button(By.XPath($"//div[@id='center-toolbar']//div[@id='js-rating-buttons']/button[@data-value='{stars}']"));
            ratingButton.Click();
            WaitForCondition(() => !ratingButton.IsElementPresent, "Changing rating value timeout expired");
        }

        public void SetStatus(AssetStatus status)
        {
            GetActionButton(AssetAction.SetStatus).Click();
            var statusButton = new Button(By.XPath($"//div[@id='center-toolbar']//div[@id='js-status-buttons']/button[@data-value='{status.GetAdditionalInfo()}']"));
            statusButton.Click();
            WaitForCondition(() => !statusButton.IsElementPresent, "Changing status value timeout expired");
        }
    }
}
