﻿using AutomationCourseFramework.Pages.PageElements;
using AutomationCourseFramework.WebDriver;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.Archives
{
    public class ArchivePageItem : BasePage
    {
        private readonly string _rootXpath;
        internal ArchivePageItem(int index)
        {
            _rootXpath = $"//div[@id='thumbnailWrapperSimple']//ul[@class='archive-list media-sets']/li[@class='elem media-set-elem'][{index}]";
        }

        public Label Name => new Label(By.XPath($"{_rootXpath}//div[@class='title']"));

        public void Open()
        {
            WebDriver.FindElement(By.XPath($"{_rootXpath}/a")).Click();
        }
    }
}
