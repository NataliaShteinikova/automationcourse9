﻿using AutomationCourseFramework.Common;

namespace AutomationCourseFramework.Pages.Assets.AssetActions
{
    public enum AssetAction
    {
        [ExtendedName("Rename")]
        Rename,

        [ExtendedName("Delete")]
        Delete,

        [ExtendedName("Duplicate")]
        Duplicate,

        [ExtendedName("Move to")]
        MoveTo,

        [ExtendedName("Copy to")]
        CopyTo,

        [ExtendedName("Set note")]
        SetNote,

        [ExtendedName("Set status")]
        SetStatus,

        [ExtendedName("Set rating")]
        SetRating
    }
}
